#!/usr/bin/env python

"""Package entry point."""


from lolaml.cli import cli


if __name__ == '__main__':  # pragma: no cover
    cli()
