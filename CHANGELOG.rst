Changelog
================================================================================

The changelog of ``LolaML``.
All notable changes to this project will be documented in this file.



[0.0.5] - YYYY-MM-DD - Unreleased
--------------------------------------------------------------------------------


[0.0.4] - 2019-03-02
--------------------------------------------------------------------------------

 - **[ENH]** Add ``run.log_metrics()`` to log multiple metrics.
 - **[ENH]** Add ``summary()`` to ``Run`` and ``RunData``.
 - **[ENH]** Add ``wip_mode`` to ``Run``.
 - **[ENH]** ``remote_location`` is stored in ``RunData``.
 - **[FIX]** Various little fixes.
 - **[DOC]** Add "Why Lola", "Roadmap", and "Workflow".
 - **[DOC]** Restructure and tweak docs; fix typos.


[0.0.3] - 2019-02-20
--------------------------------------------------------------------------------

 - Add LICENSE.
 - Add PyTorch+MNIST example.
 - Simplify handling of paths:
   {prefix_path or tmpdir}/{project}/{run_id} is the path from now on
 - Log the CWD.
 - Small cleanups.


[0.0.2] - 2019-02-17
--------------------------------------------------------------------------------

 - First version of lolaml
 - Basic logging: params, metrics, tags, git, call info, and general info.
 - Simple json representation of the run
 - Auto upload into the cloud
 - Initial version on PyPI
