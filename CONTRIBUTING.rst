============
Contributing
============

Setup
=======

Requirements
---------------

* Make:
    * macOS: ``$ xcode-select --install``
    * Linux: https://www.gnu.org/software/make
    * Windows: https://mingw.org/download/installer
* Python: ``$ pyenv install``
* Poetry: https://poetry.eustace.io/docs/#installation

Clone the project::

    $ git remote add origin git@github.com:sotte/lolaml.git
    $ cd lolaml

To confirm these system dependencies are configured correctly::

    $ make doctor

Installation
---------------

Install project dependencies into a virtual environment::

    $ make install

Development Tasks
===================

Manual
------

Run the tests::

    $ make test

Run static analysis::

    $ make check

Build the documentation::

    $ make docs

Automatic
---------

Keep all of the above tasks running on change::

    $ make watch

> In order to have OS X notifications, `brew install terminal-notifier`.

Continuous Integration
========================

The CI server will report overall build status::

    $ make ci

Release Tasks
===============

Release to PyPI:

- Create release branch: ``$ git checkout -b prepare_release_v<version>``
- Increase version number: ``$ poetry version``
- Set version number in sphinx: search for ``version`` and ``release`` in ``docs/conf.py``
- Update `CHANGELOG.rst`
- Git:
  ``$ git commit -m "Release v<version>"``
  and
  ``$ git push``
- Check CI
- Merge branch into master
- Git:
  ``$ git tag -a v<version>``
  and
  ``$ git push origin v<version>``
- Upload to PyPI: ``$ make upload``
