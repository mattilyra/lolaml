import json
from pathlib import Path
from typing import Dict

import pytest

import lolaml as lola
from lolaml.utils import ls_files


########################################################################################
# FIXTURES
@pytest.fixture
def basic_run(tmpdir):
    with lola.Run(path_prefix=str(tmpdir), ignore_config=True) as run:

        run.log_metric("train_acc", 0.1, step=1)
        run.log_metric("val_acc", 0.22, step=1)
        run.log_metric("train_acc", 0.3, step=2)
        run.log_metric("val_acc", 0.333, step=2)

        run.log_metric("val_acc", 0.1)

        run.log_param("lr", 0.1)
        run.log_param("epoch", 0.1)

        run.log_tag("TEST")
    return run


########################################################################################
# HELPRES
def is_subset_dict(subset_dict: Dict, superset_dict: Dict) -> bool:
    """Check if all items in `subset_dict` are part of `subperset_dict`."""
    superset = superset_dict.items()
    return all(key_value in superset for key_value in subset_dict.items())


########################################################################################
def test_high_level_run(tmpdir):
    with lola.run(path_prefix=str(tmpdir), ignore_config=True) as run:

        assert run.data.status == "running"
        assert isinstance(run.data.git["sha"], str)
        assert isinstance(run.data.git["diff"], str)
        assert isinstance(run.data.git["status"], str)

        assert len(run.data.metrics) == 0
        run.log_metric("train_acc", 0.1, step=1)
        run.log_metric("val_acc", 0.22, step=1)
        run.log_metric("train_acc", 0.3, step=2)
        run.log_metric("val_acc", 0.333, step=2)
        assert len(run.data.metrics) == 4

        run.log_param("lr", 0.1)
        assert run.data.params["lr"] == 0.1

        run.log_tag("WIP")
        assert run.data.tags == ["WIP"]

        assert run.data.status == "running"
    assert run.data.status == "done"


########################################################################################
# DIFFERENT LOGGERS
def test_log_param():
    with lola.run(ignore_config=True) as run:
        run.log_param("lr", 0.1)
    assert run.data.params["lr"] == 0.1


def test_log_params():
    with lola.run(ignore_config=True) as run:
        run.log_params({"lr": 0.1, "epochs": 10})
        assert run.data.params["lr"] == 0.1
        assert run.data.params["epochs"] == 10


def test_log_metrics():
    with lola.run(ignore_config=True) as run:
        run.log_metric("acc", 0.8, step=1)
        run.log_metric("acc", 0.9, step=2)
        run.log_metric("loss", 0.1)

    data = run.data
    assert len(data.metrics) == 3
    assert is_subset_dict({"name": "acc", "value": 0.8, "step": 1}, data.metrics[0])
    assert is_subset_dict({"name": "acc", "value": 0.9, "step": 2}, data.metrics[1])
    assert is_subset_dict({"name": "loss", "value": 0.1, "step": None}, data.metrics[2])


def test_log_metric():
    with lola.run(ignore_config=True) as run:
        run.log_metrics({"acc": 0.8, "loss": 0.4}, step=1)

    data = run.data
    assert len(data.metrics) == 2
    assert is_subset_dict({"name": "acc", "value": 0.8, "step": 1}, data.metrics[0])
    assert is_subset_dict({"name": "loss", "value": 0.4, "step": 1}, data.metrics[1])


def test_log_tag():
    with lola.run(ignore_config=True) as run:
        assert len(run.data.tags) == 0
        run.log_tag("TEST")
        assert len(run.data.tags) == 1
        assert "TEST" in run.data.tags

        run.log_tag("TEST")
        assert len(run.data.tags) == 1

        run.log_tag("WIP")
        assert "WIP" in run.data.tags
        assert len(run.data.tags) == 2


def test_log_call_info():
    with lola.Run(log_call_info=True, ignore_config=True) as run:
        assert isinstance(run.data.call_info, dict)
        assert run.data.call_info["__file__"].endswith("lolaml/core.py")
        assert "argv" in run.data.call_info


def test_dont_log_call_info():
    with lola.Run(log_call_info=False, ignore_config=True) as run:
        assert run.data.call_info is None


def test_log_artifact_that_does_not_exist():
    with lola.Run(ignore_config=True) as run:
        run._log_artifact("non_existing_file.txt")

    assert "non_existing_file.txt" in run.data.artifacts


def test_log_artifact_that_is_a_file(tmpdir):
    with lola.Run(path_prefix=tmpdir, ignore_config=True) as run:
        dummy_path = Path(run.path) / "dummy.txt"
        with dummy_path.open("w") as f:
            f.write("Hellola")
        run._log_artifact(str(dummy_path))

    p = str(dummy_path)
    assert p in run.data.artifacts
    for k in ["type", "st_size", "st_atime", "st_mtime", "st_ctime"]:
        assert k in run.data.artifacts[p]
    assert run.data.artifacts[p]["type"] == "file"


def test_log_all_artifacts(tmpdir):
    with lola.Run(path_prefix=tmpdir, ignore_config=True) as run:
        p = Path(run.path)
        files = [p / "dummy.txt", p / "metrics.txt", p / "models" / "best.txt"]
        for file in files:
            file.parent.mkdir(exist_ok=True, parents=True)
            with file.open("w") as f:
                f.write("foo")
        run._log_all_artifacts()

    assert len(run.data.artifacts) == len(files) + 1  # the log file


def test_summary(basic_run):
    # just make sure that it runs :)
    assert isinstance(basic_run.summary(), str)


########################################################################################
# REMOTE STORAGE
def test_remote_storage_with_local_destination(tmpdir):
    path_prefix = tmpdir / "artifact_location"
    path_prefix.mkdir()
    remote_path = tmpdir / "remote_path"
    remote_path.mkdir()

    with lola.Run(
        path_prefix=path_prefix, remote_location=str(remote_path), ignore_config=True
    ) as run:
        dummy_path = Path(run.path) / "dummy.txt"
        with dummy_path.open("w") as f:
            f.write("Hellola")

    files = ls_files(remote_path)
    assert any(f.name == "dummy.txt" for f in files)
    assert any(f.name == "lola_run.json" for f in files)
    assert len(files) == 2


########################################################################################
# WRITE LOG
def test_write_log_if_path_is_set(tmpdir):
    run = lola.run(path_prefix=str(tmpdir), ignore_config=True)
    run.close()
    run_logs = list(Path(tmpdir).rglob("lola_run.json"))
    assert len(run_logs) == 1


def test_write_log_if_path_prefix_is_set(tmpdir):
    with lola.Run(path_prefix=str(tmpdir), ignore_config=True) as run:
        assert isinstance(run.path, str)
    run_logs = list(Path(tmpdir).rglob("lola_run.json"))
    assert len(run_logs) == 1


def test_write_log_without_path_prefix_set(tmpdir):
    # This should write to an automatically created temporary dir
    with lola.Run(path_prefix=None, ignore_config=True) as run:
        assert isinstance(run.path, str)
    run_logs = list(Path(run.path).rglob("lola_run.json"))
    assert len(run_logs) == 1


def test_write_log_with_context_manager_class(tmpdir):
    with lola.Run(path_prefix=str(tmpdir), ignore_config=True) as _:
        pass
    run_logs = list(Path(tmpdir).rglob("lola_run.json"))
    assert len(run_logs) == 1


def test_write_log_with_context_manager_function(tmpdir):
    with lola.run(path_prefix=str(tmpdir), ignore_config=True) as _:
        pass
    run_logs = list(Path(tmpdir).rglob("lola_run.json"))
    assert len(run_logs) == 1


def test_context_manager_with_exception(tmpdir):
    with pytest.raises(ValueError):
        with lola.run(path_prefix=str(tmpdir), ignore_config=True) as _:
            # pretend there is some error
            raise ValueError

    run_logs = list(Path(tmpdir).rglob("lola_run.json"))
    assert len(run_logs) == 1
    with run_logs[0].open() as f:
        data = json.load(f)
    print(list(data.keys()))

    assert data["status"] == "error"


########################################################################################
# JSON STORAGE
def test_json_storage_read_write(basic_run):
    data = basic_run.data
    data2 = basic_run._storage.read(data.run_file)

    assert data.project == data2.project
    assert data.run_id == data2.run_id
    assert data == data2
