Lola + PyTorch + MNIST
======================

This is a simple PyTorch+MNIST+Lola example.
It is taken from https://github.com/pytorch/examples/tree/master/mnist,
cleaned up a bit, and adjusted to use lola.


Requirements
------------

Additionally to lola you need `pytorch` v1.0 and `torchvision`::

    pip install https://download.pytorch.org/whl/cpu/torch-1.0.1.post2-cp36-cp36m-linux_x86_64.whl
    pip install torchvision

