import os
from functools import partial
from pprint import pprint
from typing import Dict
from dataclasses import asdict

import click
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms

import lolaml as lola


MNIST_PATH = "data/mnist"
EXPERIMEN_PATH = "data/experiments"


# THE MODEL
class ConvNet(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(1, 20, 5, 1)
        self.conv2 = nn.Conv2d(20, 50, 5, 1)
        self.fc1 = nn.Linear(4 * 4 * 50, 500)
        self.fc2 = nn.Linear(500, 10)

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = F.max_pool2d(x, 2, 2)
        x = F.relu(self.conv2(x))
        x = F.max_pool2d(x, 2, 2)
        x = x.view(-1, 4 * 4 * 50)
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return F.log_softmax(x, dim=1)


# THE DATA
def get_data(params: Dict, use_cuda):
    kwargs = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
    train_ds = datasets.MNIST(
        MNIST_PATH,
        train=True,
        download=True,
        transform=transforms.Compose(
            [transforms.ToTensor(), transforms.Normalize((0.1307,), (0.3081,))]
        ),
    )
    train_dl = torch.utils.data.DataLoader(
        train_ds, batch_size=params["batch_size"], shuffle=True, **kwargs
    )

    test_ds = datasets.MNIST(
        MNIST_PATH,
        train=False,
        transform=transforms.Compose(
            [transforms.ToTensor(), transforms.Normalize((0.1307,), (0.3081,))]
        ),
    )
    test_dl = torch.utils.data.DataLoader(
        test_ds, batch_size=params["test_batch_size"], shuffle=True, **kwargs
    )

    return train_dl, test_dl


# TRAIN FUNCTION
def train(model, loss_fn, optimizer, train_dl, epoch, device, params):
    total_loss, correct = 0.0, 0
    model.train()
    for batch_idx, (data, target) in enumerate(train_dl):
        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()
        output = model(data)
        loss = F.nll_loss(output, target)
        loss.backward()
        optimizer.step()

        # Stats
        total_loss += loss
        pred = output.argmax(dim=1, keepdim=True)
        correct += pred.eq(target.view_as(pred)).sum().item()

        if batch_idx % params["log_interval"] == 0:
            _acc = 100.0 * batch_idx / len(train_dl)
            print(
                f"Train Epoch: {epoch} [{batch_idx * len(data)}/{len(train_dl.dataset)} ({_acc:.0f}%)]\tLoss: {loss.item():.6f}"
            )

    loss = total_loss / len(train_dl.dataset)
    accuracy = 100.0 * correct / len(train_dl.dataset)
    return float(loss), float(accuracy)


# TEST FUNCTION
def test(model, test_dl, device, params):
    total_loss, correct = 0.0, 0
    model.eval()
    with torch.no_grad():
        for data, target in test_dl:
            data, target = data.to(device), target.to(device)
            output = model(data)

            # Stats
            total_loss += F.nll_loss(output, target, reduction='sum').item()
            pred = output.argmax(dim=1, keepdim=True)
            correct += pred.eq(target.view_as(pred)).sum().item()

    loss = total_loss / len(test_dl.dataset)
    accuracy = 100.0 * correct / len(test_dl.dataset)

    print()
    print(
        f"Test set: Average loss: {loss:.4f}, Accuracy: {correct}/{len(test_dl.dataset)} ({accuracy:.0f})"
    )
    print()
    return float(loss), float(accuracy)


########################################################################################
# CLI
click.option = partial(click.option, show_default=True)  # Always show click defaults


@click.command()
@click.option(
    "--batch_size", type=int, default=64, help="input batch size for training"
)
@click.option(
    "--test_batch_size", type=int, default=1000, help="input batch size for testing"
)
@click.option("--epochs", type=int, default=10, help="number of epochs to train")
@click.option("--lr", type=float, default=0.01, help='learning rate')
@click.option("--momentum", type=float, default=0.5, help="SGD momentum")
@click.option("--seed", type=int, default=1, help="random seed")
@click.option(
    "--log_interval", type=int, default=10, help="Interval in batches between logging"
)
def main(batch_size, test_batch_size, epochs, lr, momentum, seed, log_interval):
    """MNIST training with pytorch and lola."""
    params = locals()

    with lola.Run(project="mnist", path_prefix=EXPERIMEN_PATH) as run:
        run.log_tags("DEMO", "MNIST")

        print("# PARAMS")
        pprint(params)
        run.log_params(params)

        # INIT
        torch.manual_seed(params["seed"])
        use_cuda = torch.cuda.is_available()
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

        # DATA
        print("# DATA")
        train_dl, test_dl = get_data(params, use_cuda)

        # MODEL, LOSS, OPTIMIZER
        model = ConvNet().to(device)
        optimizer = optim.SGD(
            model.parameters(), lr=params["lr"], momentum=params["momentum"]
        )
        loss_fn = F.nll_loss

        # TRAIN and EVAL
        print("# TRAIN and EVAL")
        for epoch in range(1, params["epochs"] + 1):
            loss, acc = train(
                model, loss_fn, optimizer, train_dl, epoch, device, params
            )
            run.log_metric("train_loss", loss, step=epoch)
            run.log_metric("train_acc", acc, step=epoch)

            loss, acc = test(model, test_dl, device, params)
            run.log_metric("test_loss", loss, step=epoch)
            run.log_metric("test_acc", acc, step=epoch)

        # Everything under run.path is automatically logged
        torch.save(model.state_dict(), os.path.join(run.path, "mnist_cnn.pt"))

    print("# DONE")
    # This is what lola actually logged
    pprint(asdict(run.data))


if __name__ == '__main__':
    main()
