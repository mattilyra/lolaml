#!/usr/bin/env bash

python train.py \
  --batch_size 256 \
  --test_batch_size 1000 \
  --epochs 5 \
  --lr 0.01 \
  --momentum 0.5 \
  --seed 1 \
  --log_interval 20

python train.py \
  --batch_size 128 \
  --test_batch_size 1000 \
  --epochs 5 \
  --lr 0.01 \
  --momentum 0.5 \
  --seed 1 \
  --log_interval 20

python train.py \
  --batch_size 64 \
  --test_batch_size 1000 \
  --epochs 5 \
  --lr 0.01 \
  --momentum 0.5 \
  --seed 1 \
  --log_interval 20

python train.py \
  --batch_size 32 \
  --test_batch_size 1000 \
  --epochs 5 \
  --lr 0.01 \
  --momentum 0.5 \
  --seed 1 \
  --log_interval 20
