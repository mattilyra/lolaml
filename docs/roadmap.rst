Roadmap
================================================================================

Currently (2018-03) ``LolaML`` is pretty basic.
We want to get the basic logging and some basic dashboard right,
before we do the fancy things.
Feel free to help us to prioritize the features and implement them!

Here is a rough roadmap (as of <2019-02-28 Tue>):

- Add more logging features
  - Automatically log stdout
  - Log infos about the dataset
  - Log notes
  - Log misc key/values
  - Log the file type of artifacts. This would allow displaying image artifacts.
- More docs
- More examples
  - sklearn
  - keras
  - tensorflow
  - hyper param search
  - Auto upload
- Library specific integration
  - sklearn
  - pytorch
  - keras
  - fastai
- Sync JSON data to Google Firestore to get realtime sync
  - Provide nice query API
- Visualizations:
  - What visualizations are really necessary? There are so many possible plots.
  - Parallel coordinate plot
  - Add a simple dashboard notebook that is hackable
  - Add a local web GUI dashboard
  - Hosted dashboard
- Integrate hyper parameter search through existing packages.
