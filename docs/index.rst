.. LolaML documentation master file, created by
   sphinx-quickstart on Fri Feb 15 17:07:57 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: ../README.rst

Table of Contents
==================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   getting_started
   examples
   api
   roadmap
   changelog
   contributing
   license



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
