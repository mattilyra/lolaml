API
===================================

lolaml.Run
-----------

.. automodule:: lolaml
.. autoclass:: Run
   :members:


lolaml.data.RunData
-------------------

.. automodule:: lolaml.data
.. autoclass:: RunData
   :members:
