Getting Started
================================================================================


Why ``LolaML``? - Rationale
--------------------------------------------------------------------------------

Machine Learning can be used to achieve really impressive results.
However, in practice Machine Learning is an iterative process that consists of many tiny steps,
most of them in the wrong and some in the right direction.
It is as important to know how to achieve good results
as it is to know what did not work.

Nowadays it's very easy to run an experiment::

    model.fit(X, y)

but evaluating and tracking of the experiment is an afterthought
(google spreadsheet or some ad-hoc code).

``LolaML`` strives

- to be a simple library that allows you to easily track your experiments
  without imposing too many changes on your existing code,
- to scale from a single developer thats runs experiments locally,
  to teams that work on multiple projects at the same time
  and run big hyper parameter searches in the cloud (or on prem).
- to be hackable and empower the data scientist to scratch their own itches,
  i.e. just write a few lines of code to create exactly the visualization you
  need for your project.


Tracking with Lolas Friends
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Lola does not care if you go on a run with her, just track your experiments!
Maybe some other tools work better for your use case.
Here are some:

- weights and biases
- comet.ml
- Sacred
- Sumatra
- Polyaxon
- randopt
- studio.ml
- TODO Feel free to open a PR and add more trackers!



Workflow and Tracking with ``LolaML``
--------------------------------------------------------------------------------

This section describes how you can work with ``LolaML``.
The workflows are only suggestions.


Tracking with git
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Assuming you work solo or in a small team,
you only create a moderate number of experiments,
then you can simply use git to track your experiments.

Always use `lolaml.Run` to track all your experiments.
Maybe use the ``wip_mode`` (TODO link).
Each run creates a ``lola_run.json`` file.

Here is a suggestion of a ``.gitignore`` file that ignores all artifacts except
the ``loal_run.json`` under ``data/experiments/my_project/*/``

::

    #.gitignore file
    # See https://git-scm.com/docs/gitignore for wildcards
    /data/experiments/my_project/*/*
    !/data/experiments/my_project/**/lola_run.json

After a run you can add the created ``lola_run.json``::

    $ git add data/experiments/my_project
    $ git commit -m "LOLA Experiment foo"

Note if you have many experiments tracking all experiments with git will not
scale!

Tracking with a remote bucket
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Assuming you work solo or in a team,
you create any number of experiments,
and you use auto upload feature of Lola,
then you can download all ``lola_run.json`` files from the remote bucket.

TODO add script for this.


Tracking with a remote database
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

TODO Not implemented yet. Coming soon. Sooner if you help out :)


Working on a remote machine
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you run your experiments on a remote machine you can follow any of the
workflows above.



Configuration
--------------------------------------------------------------------------------

.. automodule:: lolaml.config



Auto Upload Artifacts
--------------------------------------------------------------------------------

Lola can automatically upload all artifact to a storage bucket in the cloud
at the end of a run.
This is quite useful if you train your models on an ephemeral cloud instance.

Assuming you already have a storage bucket and write access to it,
all you have to do is to specify the
``remote_location``
(the bucket where to store the artifacts)
and the ``remote_credentials``
(the path to the account/key/whatever that grants you the appropriate permission).

You can do that with code::

    import lolaml as lola

    with lola.Run(
        remote_location="gs://somewhere",
        remote_credentials="path/to/service_account.json",
    ) as run:
        # train and track ...
    ...

Or you can do that via the config ``.lola.toml``::

    [lola]
    remote_location = "gs://somewhere
    remote_credentials = "path/to/service_account.json"

The configuration options are picked up by the ``Run`` class automatically.


Supported Storage Providers
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Lola uses `cloudstorage <https://github.com/scottwernervt/cloudstorage/>`_
and therefore supports all storage providers that are supported by `cloudstorage`:

- Amazon S3
- Google Cloud Storage
- Microsoft Azure Storage
- Minio Cloud Storage
- Rackspace CloudFiles
- Local File System

(Currently, only Google Cloud Storage is tested though.)
