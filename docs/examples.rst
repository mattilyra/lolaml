Examples
================================================================================

PyTorch + MNIST Example
--------------------------------------------------------------------------------


This is a simple PyTorch+MNIST+Lola example.
It is taken from the
`official MNIST PyTorch example <https://github.com/pytorch/examples/tree/master/mnist>`_,
cleaned up a bit, and adjusted to use Lola.

Pay attention to all the ``run.log_*`` methods.

You can find the full example
`here <https://gitlab.com/stefan-otte/lolaml/tree/master/examples/pytorch_mnist_simple>`_.

.. literalinclude:: ../examples/pytorch_mnist_simple/train.py
   :language: python
