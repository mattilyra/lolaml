License
=======

This package is free to use for commercial purposes for a trial period under the terms of the Prosperity Public License.

Licenses for long-term commercial use are available via `licensezero.com <https://licensezero.com/ids/d26169f5-d688-425f-b10b-d7e85c73952f>`_.

.. image:: https://licensezero.com/projects/d26169f5-d688-425f-b10b-d7e85c73952f/badge.svg
   :target: https://licensezero.com/projects/d26169f5-d688-425f-b10b-d7e85c73952f
   :alt: licensezero.com pricing


Prosperity Public License
-------------------------

.. include:: ../LICENSE

